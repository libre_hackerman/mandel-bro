CC = gcc
SHELL = /bin/sh
CFLAGS += -I include -Wall -Werror -Wextra
LDFLAGS += -lpthread $(shell pkgconf --libs ncurses flint)

NAME = mandel-bro

SRCS:= $(shell find src -name "*.c")
OBJS = $(SRCS:.c=.o)

all: $(NAME)

$(NAME): $(OBJS)
	$(CC) $(CFLAGS) -o $(NAME) $(OBJS) $(LDFLAGS)

.o: .c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	find . -name "*.o" -delete -print

fclean: clean
	rm -f $(NAME)

re: fclean all

debug: CFLAGS += -g
debug: fclean $(NAME)

fast: CFLAGS += -march=native -O2
fast: fclean $(NAME)

.PHONY: all clean fclean re debug fast
