#ifndef MANDELBROT_H
#define MANDELBROT_H

#include <parse_args.h>
#include <flint/acb.h>

struct params
{
	unsigned int columns;
	unsigned int lines;
	fmpz_t h_offset;
	fmpz_t v_offset;
	arb_t zoom;
	int n_zooms;
	unsigned int max_iterations;
	long precision;
	int (*divergence_func)(unsigned int, unsigned int, const struct params* params);

	/* Julia specific */
	acb_t julia_c;
};

struct params*
init_params(const struct args* args, int fractal_cols, int fractal_lines);

void
delete_params(struct params* params);

int
mandelbrot_divergence(unsigned int col, unsigned int line, const struct params* params);

int
julia_divergence(unsigned int col, unsigned int line, const struct params* params);

void
set_julia_rot_c(acb_t julia_c, unsigned int step, const struct params* params);

int
is_module_smaller_or_equal_than_2(acb_t c, long precision);

#endif
