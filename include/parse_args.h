#ifndef PARSE_ARGS
#define PARSE_ARGS

struct args
{
	unsigned int iterations;
	char fractal[20];
	double julia_c_real;
	double julia_c_img;
};

int
parse_args(int argc, char* argv[], struct args* args);

#endif
