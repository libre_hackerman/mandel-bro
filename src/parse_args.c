#include <parse_args.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

/*
 * Just displays the help on stderr.
 */
static void
display_help(char* argv[])
{
	fprintf(stderr, "USAGE: %s [-i ITERATIONS] [-f FRACTAL] [-c REAL IMG] [-h]\n", argv[0]);
	fprintf(stderr, "\nOptions:\n"
					"-i ITERATIONS: sets the maximum iterations for fractal calculation\n"
					"-f FRACTAL: the fractal set. Available list below\n"
					"-c REAL IMG: for the Julia set, it specifies the c constant in f(z) = z^2 + c\n"
					"-h: displays this help and exits\n"
					"\nAvailable fractals:\n"
					"\t- mandelbrot\n"
					"\t- julia\n"
					"\t- julia_rot\n");
}

/*
 * This function parses the program arguments storing them into args and
 * returning 1. Returns 0 on error.
 */
int
parse_args(int argc, char* argv[], struct args* args)
{
	int index = 0;

	args->iterations = 100;
	strcpy(args->fractal, "mandelbrot");
	args->julia_c_real = -0.8;
	args->julia_c_img = 0.156;

	while (++index < argc)
	{
		if (strcmp(argv[index], "-i") == 0)
		{
			if (++index == argc || !isdigit(argv[index][0]))
			{
				display_help(argv);
				return (0);
			}
			args->iterations = atoi(argv[index]);
		}
		else if (strcmp(argv[index], "-f") == 0)
		{
			if (++index == argc)
			{
				display_help(argv);
				return (0);
			}
			strncpy(args->fractal, argv[index], sizeof(args->fractal)-1);
		}
		else if (strcmp(argv[index], "-c") == 0)
		{
			if (argc - index < 3)
			{
				display_help(argv);
				return (0);
			}
			args->julia_c_real = strtod(argv[++index], NULL);
			args->julia_c_img = strtod(argv[++index], NULL);
		}
		else
		{
			display_help(argv);
			return (0);
		}
	}
	return (1);
}
