#include <fractals.h>

/*
 * This function is equivalent to: |c| <= 2
 */
int
is_module_smaller_or_equal_than_2(acb_t c, long precision)
{
	arf_t c_module;
	arb_t c_module_arb, two;
	int result;

	/* (creall(c) * creall(c)) + (cimagl(c) * cimagl(c)) <= 2 * 2 */

	arf_init(c_module);
	acb_get_abs_ubound_arf(c_module, c, precision);
	arb_init(c_module_arb);
	arb_set_arf(c_module_arb, c_module);
	arf_clear(c_module);

	arb_init(two);
	arb_set_ui(two, 2);

	result = arb_le(c_module_arb, two);

	arb_clear(c_module_arb);
	arb_clear(two);
	return result;
}
