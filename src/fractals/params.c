#include <fractals.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

/*
 * Creates and initializes the fractal parameters to their starting and default values.
 * Returns NULL on error.
 */
struct params*
init_params(const struct args* args, int fractal_cols, int fractal_lines)
{
	struct params* params;

	if (!(params = malloc(sizeof(struct params))))
	{
		perror(__FUNCTION__);
		return (NULL);
	}

	arb_init(params->zoom);
	arb_one(params->zoom);
	params->n_zooms = 0;
	fmpz_init(params->h_offset);
	fmpz_init(params->v_offset);
	fmpz_zero(params->h_offset);
	fmpz_zero(params->v_offset);
	params->max_iterations = args->iterations;
	params->precision = 256;
	params->columns = fractal_cols;
	params->lines = fractal_lines;

	if (strcmp(args->fractal, "mandelbrot") == 0)
		params->divergence_func = mandelbrot_divergence;
	else if (strcmp(args->fractal, "julia") == 0 || strcmp(args->fractal, "julia_rot") == 0)
		params->divergence_func = julia_divergence;
	else
	{
		fprintf(stderr, "Invalid fractal \"%s\". Valid fractals are:\n"
				"- mandelbrot\n",
				args->fractal);
		return (NULL);
	}

	acb_init(params->julia_c);
	if (strcmp(args->fractal, "julia_rot") == 0)
		set_julia_rot_c(params->julia_c, 0, params);
	else
		acb_set_d_d(params->julia_c, args->julia_c_real, args->julia_c_img);

	return (params);
}

/*
 * Frees the resouces used by a struct params
 */
void
delete_params(struct params* params)
{
	arb_clear(params->zoom);
	fmpz_clear(params->h_offset);
	fmpz_clear(params->v_offset);
	acb_clear(params->julia_c);
	free(params);
}
