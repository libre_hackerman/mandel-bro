#include <fractals.h>
#include <flint/acb.h>

/*
 * This function receives a given position and the calculation parameters and
 * returns the number of iterations before the Mandelbrot sequence goes
 * beyond 2 (diverges). Returns -1 if the sequence *never*
 * (depending on params->max_iterations) goes beyond 2 (converges).
 */
int
mandelbrot_divergence(unsigned int col, unsigned int line, const struct params* params)
{
	arb_t step_cols, step_lines, starting_real_c, starting_img_c;
	acb_t c, z;
	int iteration = -1;

	/* step_cols = 4.0 / params->columns * params->zoom; */
	arb_init(step_cols);
	arb_set_d(step_cols, 4.0);
	arb_div_ui(step_cols, step_cols, params->columns, params->precision);
	arb_mul(step_cols, step_cols, params->zoom, params->precision);

	/* step_lines = 2.0 / params->lines * params->zoom; */
	arb_init(step_lines);
	arb_set_d(step_lines, 2.0);
	arb_div_ui(step_lines, step_lines, params->lines, params->precision);
	arb_mul(step_lines, step_lines, params->zoom, params->precision);

	/*
	c = (col - params->columns / 2.0 + params->h_offset) * step_cols; <-- Real
	c += (params->lines / 2.0 - line + params->v_offset) * step_lines * I; <-- Img
	*/
	arb_init(starting_real_c);
	arb_set_d(starting_real_c, col - params->columns / 2.0);
	arb_add_fmpz(starting_real_c, starting_real_c, params->h_offset, params->precision);
	arb_mul(starting_real_c, starting_real_c, step_cols, params->precision);

	arb_init(starting_img_c);
	arb_set_d(starting_img_c, params->lines / 2.0 - line);
	arb_add_fmpz(starting_img_c, starting_img_c, params->v_offset, params->precision);
	arb_mul(starting_img_c, starting_img_c, step_lines, params->precision);

	acb_init(c);
	acb_set_arb_arb(c, starting_real_c, starting_img_c);
	arb_clear(starting_real_c);
	arb_clear(starting_img_c);
	arb_clear(step_cols);
	arb_clear(step_lines);

	acb_init(z);
	acb_zero(z);
	for (unsigned int it = 0; it < params->max_iterations; it++)
	{
		if (!is_module_smaller_or_equal_than_2(z, params->precision))
		{
			iteration = it;
			goto ret_iteration;
		}
		/* z = z * z + c; */
		acb_mul(z, z, z, params->precision);
		acb_add(z, z, c, params->precision);
	}

ret_iteration:
	acb_clear(c);
	acb_clear(z);
	return iteration;
}
