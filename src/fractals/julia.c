#include <fractals.h>
#include <flint/acb.h>

/*
 * This function receives a given position and the calculation parameters and
 * returns the number of iterations before the Julia sequence goes
 * beyond 2 (diverges). Returns -1 if the sequence *never*
 * (depending on params->max_iterations) goes beyond 2 (converges).
 */
int
julia_divergence(unsigned int col, unsigned int line, const struct params* params)
{
	arb_t step_cols, step_lines, starting_real_z, starting_img_z;
	acb_t z;
	int iteration = -1;

	/* step_cols = 4.0 / params->columns * params->zoom; */
	arb_init(step_cols);
	arb_set_d(step_cols, 4.0);
	arb_div_ui(step_cols, step_cols, params->columns, params->precision);
	arb_mul(step_cols, step_cols, params->zoom, params->precision);

	/* step_lines = 4.0 / params->lines * params->zoom; */
	arb_init(step_lines);
	arb_set_d(step_lines, 4.0);
	arb_div_ui(step_lines, step_lines, params->lines, params->precision);
	arb_mul(step_lines, step_lines, params->zoom, params->precision);

	/*
	z = (col - params->columns / 2.0 + params->h_offset) * step_cols; <-- Real
	z += (params->lines / 2.0 - line + params->v_offset) * step_lines * I; <-- Img
	*/
	arb_init(starting_real_z);
	arb_set_d(starting_real_z, col - params->columns / 2.0);
	arb_add_fmpz(starting_real_z, starting_real_z, params->h_offset, params->precision);
	arb_mul(starting_real_z, starting_real_z, step_cols, params->precision);

	arb_init(starting_img_z);
	arb_set_d(starting_img_z, params->lines / 2.0 - line);
	arb_add_fmpz(starting_img_z, starting_img_z, params->v_offset, params->precision);
	arb_mul(starting_img_z, starting_img_z, step_lines, params->precision);

	acb_init(z);
	acb_set_arb_arb(z, starting_real_z, starting_img_z);
	arb_clear(starting_real_z);
	arb_clear(starting_img_z);
	arb_clear(step_cols);
	arb_clear(step_lines);

	for (unsigned int it = 0; it < params->max_iterations; it++)
	{
		if (!is_module_smaller_or_equal_than_2(z, params->precision))
		{
			iteration = it;
			goto ret_iteration;
		}

		/* z = z * z + c; */
		acb_mul(z, z, z, params->precision);
		acb_add(z, z, params->julia_c, params->precision);
	}

ret_iteration:
	acb_clear(z);
	return iteration;
}

/*
 * Helper function for the julia_rot fractal, which sets the c constant to
 * c = 0.7885*e^(i*2*pi*prop)
 * where prop is a 0.0 to 1.0
 * The step parameter allows to set the prop constant, scaled from 0 to 100.
 */
void
set_julia_rot_c(acb_t julia_c, unsigned int step, const struct params* params)
{
	arb_t proportion, a, zero, rot_constant, euler_constant;
	acb_t exponent;

	arb_init(zero);
	arb_zero(zero);

	arb_init(proportion);
	arb_set_d(proportion, step / 100.0);

	/* a = 2*pi*proportion */
	arb_init(a);
	arb_const_pi(a, params->precision);
	arb_mul_ui(a, a, 2, params->precision);
	arb_mul(a, a, proportion, params->precision);
	arb_clear(proportion);

	/* exponent = 0 + Ai */
	acb_init(exponent);
	acb_set_arb_arb(exponent, zero, a);
	arb_clear(a);

	arb_init(rot_constant);
	arb_set_d(rot_constant, 0.7885);

	arb_init(euler_constant);
	arb_const_euler(euler_constant, params->precision);

	/* c = 0.7885 * e^exponent */
	acb_set_arb_arb(julia_c, euler_constant, zero);
	acb_pow(julia_c, julia_c, exponent, params->precision);
	acb_mul_arb(julia_c, julia_c, rot_constant, params->precision);

	arb_clear(zero);
	arb_clear(euler_constant);
	arb_clear(rot_constant);
	acb_clear(exponent);
}
