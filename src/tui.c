#include <flint/fmpz.h>
#include <tui.h>
#include <fractals.h>
#include <pthread.h>
#include <stdlib.h>
#include <sys/time.h>
#include <string.h>

#if defined(USE_PDCURSES)
#include <pdcurses.h>
#else
#include <ncurses.h>
#endif

#if defined(__linux__)
#include <sys/sysinfo.h>
#elif defined(_WIN32)
#include <windows.h>
#endif

#define PAIR_DEFAULT 1
#define PAIR_BLACK 2
#define PAIR_WHITE 9

struct thread_data
{
	struct params* params;
	int** divergences_matrix;
	unsigned int dots_offset;
	unsigned int n_dots;
};

/*
 * This function defines the Ncurses color pairs:
 * 1 (PAIR_DEFAULT): default
 * 2: (PAIR_BLACK): black
 * 3: red
 * 4: green
 * ...
 * 9: (PAIR_WHITE): white
 */
static void
set_colors()
{
	start_color();
	use_default_colors();

	init_pair(PAIR_DEFAULT, -1, -1);
	for (int i = 0; i <= 7; i++)
		init_pair(i + 2, i, -1);

	attrset(COLOR_PAIR(PAIR_DEFAULT));
}

/*
 * Given a position on the window and the iterations before divergence (with
 * the maxium of iterations used to calculate it) this function places either
 * a # symbol when the position converges, and a coloured * if the position
 * diverges after a minium of iterations.
 */
static void
print_matrix(WINDOW* fractal_w, int** divergences_matrix, unsigned int max_iterations)
{
	int pair;

	for (int col = 0; col < COLS; col++)
	{
		for (int line = 0; line < LINES - 1; line++)
		{
			if (divergences_matrix[col][line] < 0)
				mvwaddch(fractal_w, line, col, '#' | COLOR_PAIR(PAIR_WHITE));
			else if (divergences_matrix[col][line] > 10)
			{
				pair = 7 * divergences_matrix[col][line] / max_iterations + 3;
				mvwaddch(fractal_w, line, col, '&' | COLOR_PAIR(pair));
			}
		}
	}
	wnoutrefresh(fractal_w);
}

/*
 * Calculates the portion of the divergences matrix given the passed struct thread_data.
 */
static void*
update_chunk(void* v_data)
{
	struct thread_data* data = (struct thread_data*)v_data;

	unsigned int skipped = 0, computed = 0;

	for (int col = 0; col < COLS; col++)
	{
		for (int line = 0; line < LINES - 1; line++)
		{
			if (skipped++ < data->dots_offset)
				continue;

			data->divergences_matrix[col][line] = data->params->divergence_func(col, line, data->params);

			if (++computed >= data->n_dots)
				return (NULL);
		}
	}
	return (NULL);
}

/*
 * This function redraws the fractal acording to the passed parameters.
 * Stores the milliseconds it took into the milles argument (MUST be writable).
 */
static void
update_fractal(WINDOW* fractal_w, struct params* params, int n_threads, unsigned long long *millies)
{
	struct thread_data* chunks = NULL;
	pthread_t* threads = NULL;
	int** divergences_matrix = NULL;
	unsigned int n_dots;
	struct timeval t1, t2;

	gettimeofday(&t1, NULL);
	if ((chunks = calloc(n_threads, sizeof(struct thread_data))) == NULL) {
		goto error;
	}
	if ((threads = calloc(n_threads, sizeof(pthread_t))) == NULL) {
		goto error;
	}
	if ((divergences_matrix = calloc(COLS, sizeof(int*))) == NULL)
		goto error;
	for (int i = 0; i < COLS; i++)
		if ((divergences_matrix[i] = calloc(LINES - 1, sizeof(int))) == NULL)
			goto error;

	n_dots = ((LINES - 1) * COLS) / n_threads;
	werase(fractal_w);
	for (int i = 0; i < n_threads; i++)
	{
		chunks[i].params = params;
		chunks[i].divergences_matrix = divergences_matrix;
		chunks[i].dots_offset = n_dots * i;
		chunks[i].n_dots = n_dots;
		if (i == n_threads - 1)
			chunks[i].n_dots += ((LINES - 1) * COLS) % n_threads;

		pthread_create(threads + i, NULL, update_chunk, chunks + i);
	}

	for (int i = 0; i < n_threads; i++)
		pthread_join(threads[i], NULL);
	gettimeofday(&t2, NULL);

	print_matrix(fractal_w, divergences_matrix, params->max_iterations);
	free(chunks);
	free(threads);
	for (int i = 0; i < COLS; i++)
		free(divergences_matrix[i]);
	free(divergences_matrix);

	*millies = ((t2.tv_sec * 1000) + (t2.tv_usec / 1000)) -
		((t1.tv_sec * 1000) + (t1.tv_usec / 1000));
	return;

error:
	perror(__FUNCTION__);
	if (chunks)
		free(chunks);
	if (threads)
		free(threads);
	if (divergences_matrix)
	{
		for (int i = 0; i < COLS; i++)
			free(divergences_matrix[i]);
		free(divergences_matrix);
	}
}

/*
 * This function updates the status windows information.
 */
static void
update_status(WINDOW* status_w,
		const struct params* params,
		const char* fractal,
		unsigned int n_threads,
		unsigned int frame_n,
		unsigned long long frame_millies)
{
	werase(status_w);
	mvwprintw(status_w, 0, 0, "Set: %s", fractal);
	if (params->divergence_func == julia_divergence)
		wprintw(status_w, " | c = %.3f+%.3fi",
				arf_get_d(arb_midref(acb_realref(params->julia_c)), ARF_RND_NEAR),
				arf_get_d(arb_midref(acb_imagref(params->julia_c)), ARF_RND_NEAR));

	mvwprintw(status_w, 1, 0, "Max iteration: %u | Precision bits: %ld | Zooms: %d | CPU threads: %u | Frame: %u | Computing time: %llu.%03llus",
			params->max_iterations, params->precision, params->n_zooms, n_threads, frame_n, frame_millies/1000, frame_millies%1000);

	/* Keys legend */
	wattron(status_w, COLOR_PAIR(4));
	mvwaddstr(status_w, 2, 0, "Movement: ARROWS, h,j,k,l | Zoom: + - | Iterations: * / | Precision: $ % | Quit: q");
	if (strcmp(fractal, "julia_rot") == 0)
		waddstr(status_w, " | Rotation step: n m");
	wattroff(status_w, COLOR_PAIR(4));
	wnoutrefresh(status_w);
}

/*
 * Input/redrawing loop.
 */
static void
mainloop(struct args* cli_args)
{
#if defined(__linux__)
	const unsigned int n_threads = get_nprocs();
#elif defined(_WIN32)
	SYSTEM_INFO sysinfo;
	GetSystemInfo(&sysinfo);
	const unsigned int n_threads = sysinfo.dwNumberOfProcessors;
#else
	const unsigned int n_threads = 2;
#endif
	int key;
	unsigned long long frame_millies = 0;
	unsigned frame_n = 0;
	unsigned int julia_rot_step = 0;

	struct params* params;
	if (!(params = init_params(cli_args, COLS, LINES - 3)))
		return;

	WINDOW* fractal_w = newwin(params->lines, params->columns, 0, 0);
	WINDOW* status_w = newwin(3, COLS, LINES - 3, 0);

	do
	{
		erase();
		wnoutrefresh(stdscr);
		update_fractal(fractal_w, params, n_threads, &frame_millies);
		update_status(status_w, params, cli_args->fractal, n_threads, frame_n++, frame_millies);
		doupdate();

		switch ((key = getch()))
		{
			case KEY_UP:
			case 'k':
				fmpz_add_ui(params->v_offset, params->v_offset, 5);
				break;
			case KEY_DOWN:
			case 'j':
				fmpz_sub_ui(params->v_offset, params->v_offset, 5);
				break;
			case KEY_LEFT:
			case 'h':
				fmpz_sub_ui(params->h_offset, params->h_offset, 5);
				break;
			case KEY_RIGHT:
			case 'l':
				fmpz_add_ui(params->h_offset, params->h_offset, 5);
				break;
			case '+':
				arb_div_ui(params->zoom, params->zoom, 2, params->precision);
				fmpz_mul_ui(params->h_offset, params->h_offset, 2);
				fmpz_mul_ui(params->v_offset, params->v_offset, 2);
				params->n_zooms++;
				break;
			case 'n':
				if (strcmp(cli_args->fractal, "julia_rot") == 0) {
					julia_rot_step = julia_rot_step == 100 ? 0 : julia_rot_step+1;
					set_julia_rot_c(params->julia_c, julia_rot_step, params);
				}
				break;
			case 'm':
				if (strcmp(cli_args->fractal, "julia_rot") == 0) {
					julia_rot_step = julia_rot_step == 0 ? 100 : julia_rot_step-1;
					set_julia_rot_c(params->julia_c, julia_rot_step, params);
				}
				break;
			case '-':
				arb_mul_ui(params->zoom, params->zoom, 2, params->precision);
				fmpz_divexact_ui(params->h_offset, params->h_offset, 2);
				fmpz_divexact_ui(params->v_offset, params->v_offset, 2);
				params->n_zooms--;
				break;
			case '*':
				params->max_iterations += 100;
				break;
			case '/':
				params->max_iterations -= 100;
				if (params->max_iterations <= 0)
					params->max_iterations = 100;
				break;
			case '$':
				params->precision += 64;
				break;
			case '%':
				params->precision -= 64;
				if (params->precision <= 0)
					params->precision = 64;
				break;
			case KEY_RESIZE:
				resize_term(0, 0);
				params->columns = COLS;
				params->lines = LINES - 2;

				delwin(fractal_w);
				delwin(status_w);
				fractal_w = newwin(params->lines, params->columns, 0, 0);
				status_w = newwin(3, COLS, LINES - 3, 0);
				break;
		}
	}
	while (key != 'q');

	delwin(status_w);
	delwin(fractal_w);
	delete_params(params);
}

/*
 * Opens the Ncurses interface of the program.
 */
void
tui(struct args* args)
{
	initscr();
	cbreak();
	noecho();
	keypad(stdscr, TRUE);
	curs_set(0);
	set_colors();

	mainloop(args);

	endwin();
}
