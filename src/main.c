#include <parse_args.h>
#include <tui.h>

int
main(int argc, char* argv[])
{
	struct args args;

	if (!parse_args(argc, argv, &args))
		return (1);

	tui(&args);
	return (0);
}
